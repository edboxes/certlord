require 'active_support'
require 'active_support/core_ext'
require 'settingslogic'
require 'aws-sdk'
require 'grape'
require 'chronic_duration'

require 'ice_nine'

require 'json'
require 'pathname'
require 'lockfile'
require 'fileutils'
require 'parallel'
require 'tempfile'

require 'wonk'
require 'certlord/version'
require 'certlord/settings'
require 'certlord/logging'

Wonk.aws_region = Certlord::Settings.aws.region
Wonk.logger = Certlord::Logging.logger

Dir["#{__dir__}/**/*.rb"] \
  .reject { |f| f.include?("/api/") } \
  .reject { |f| f.include?("/models/") } \
  .reject { |f| f.include?("models.rb") } \
  .each { |f| require_relative f }

module Certlord
end
