require 'settingslogic'

module Certlord
  class Settings < Settingslogic
    source ENV['CERTLORD_SETTINGS_PATH'] || "/etc/certlord/settings.yaml"
  end
end
