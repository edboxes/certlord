require 'certlord/api/errors'
require 'certlord/validity/writer'

require 'r509'
require 'r509/io_helpers'

require 'digest'

module Certlord
  module API
    class V1 < Grape::API
      TOKEN_HEADER_NAME = "X-Certlord-Token"

      logger Certlord::Logging.logger

      format :json
      version :v1, type: :path

      rescue_from BadRequestError do |e|
        error!(e.message, 400)
      end

      rescue_from UnauthorizedError do |e|
        error!(e.message, 401, 'WWW-Authenticate' => '/v1/authenticate')
      end

      rescue_from ForbiddenError do |e|
        error!(e.message, 403)
      end

      rescue_from NotFoundError do |e|
        error!(e.message, 404)
      end

      rescue_from InternalServerError do |e|
        error!(e.message, 500)
      end

      rescue_from NotImplementedError do |e|
        error!(e.message, 501)
      end

      helpers do
        def logger
          Certlord::Logging.logger
        end

        def token_expiry_duration
          @token_expiry_duration ||=
            ChronicDuration.parse(Certlord::Settings.security.token_duration, keep_zero: true)
        end

        def policies
          @policies =
            Dir["#{Certlord::Settings.security.policy_directory}/*.json"].map do |f|
              Wonk::Policy.from_hash(f, JSON.parse(IO.read(f)))
            end.freeze
        end

        def authenticate_from_submission(submission)
          submission = IceNine.deep_freeze(submission.deep_dup.deep_symbolize_keys)

          policy_results =
            policies.map do |p|
              [ p, p.authenticate_from_submission(submission) ]
            end.select { |pt| pt[1].success? }

          if !policy_results.empty?
            rules = policy_results.map { |pt| pt[1].concretized_content }.flatten

            token = Certlord::Models::Token.generate(rules)
            token
          else
            nil
          end
        end

        def authenticate!(&block)
          token = Models::Token.first(value: request.headers[TOKEN_HEADER_NAME])

          raise UnauthorizedError, "#{TOKEN_HEADER_NAME} not passed" if token.nil?

          if token.revoked || (token.issued_on + token_expiry_duration.seconds) < DateTime.now
            revoke_token(token)
            raise UnauthorizedError, "token is invalid"
          end

          if block.nil?
            token
          else
            block.call(token)
          end
        end

        def revoke_token(token)
          raise BadRequestError, "'token' must be a Certlord::Models::Token." \
            unless token.is_a?(Models::Token)

          token.revoked = true
          token.save
          token
        end

        def request_body_as_json
          begin
            JSON.parse(request.body.read).deep_symbolize_keys
          rescue
            raise BadRequestError, "error parsing request body - was it valid JSON?"
          end
        end

        def authorize!(authority:, subject:, action:, &block)
          authenticate! do |token|
            if token.authorized?(authority: authority, subject: subject, action: action.to_s)
              if block.nil?
                nil
              else
                yield
              end
            else
              raise ForbiddenError, "can't invoke '#{action}' on resource '#{type}' named '#{name}'."
            end
          end
        end

        def config_pool
          @config_pool ||=
            R509::Config::CAConfigPool.new(
              Certlord::Settings.certificate_authorities.map do |name, cfg|
                [ name, R509::Config::CAConfig.load_from_hash(
                    cfg.with_indifferent_access, {}) ]
              end.to_h
            )
        end

        def crls
          @crls ||=
            config_pool.names.map do |name|
              [ name, R509::CRL::Administrator.new(config_pool[name]) ]
            end.to_h
        end

        def options_builders
          @options_builders ||=
            config_pool.names.map do |name|
              [ name, R509::CertificateAuthority::OptionsBuilder.new(config_pool[name]) ]
            end.to_h
        end

        def certificate_authorities
          @certificate_authorities ||=
            config_pool.names.map do |name|
              [ name, R509::CertificateAuthority::Signer.new(config_pool[name]) ]
            end.to_h
        end

        def register_issued_certificate!(authority_name, csr, certificate)
          raise ArgumentError, "'authority_name' must be a String" \
            unless authority_name.is_a?(String)
          raise ArgumentError, "'csr' must be an R509::CSR" \
            unless csr.is_a?(R509::CSR)
          raise ArgumentError, "'certificate' must be an R509::Cert" \
            unless certificate.is_a?(R509::Cert)

          csr_hash = Digest::SHA256.hexdigest(csr.req.to_pem)

          logger.warn "duplicate issuance attempt: csr w/hash #{csr_hash}"
          raise SigningError, "csr with hash '#{csr_hash}' has already been " \
                              "signed and registered." \
            unless Models::ValidityEntry.first(
              csr_hash: csr_hash
            ).nil?

          logger.warn "duplicate issuance attempt: cert serial #{certificate.serial}"
          raise SigningError, "cert with serial '#{certificate.serial}' has already been " \
                              "registered." \
            unless Models::ValidityEntry.first(
              serial: certificate.serial
            ).nil?

          new_entry = Models::ValidityEntry.new(
            authority_name: authority_name,
            csr_hash: csr_hash,
            csr: csr.to_pem,
            serial: certificate.serial,
            certificate: certificate.to_pem,

            status: R509::Validity::VALID,
            created_on: DateTime.now
          )

          new_entry.save
          new_entry

          logger.info "Registered certificate '#{certificate.serial}' " \
                      "(CN #{csr.subject['CN']}) to database as issued."
        end
      end

      desc 'Performs a health check on the service and returns 200 with service info.'
      get 'health-check' do
        {
          version: Certlord::VERSION,
          ca_count: certificate_authorities.count
        }
      end

      desc 'Authenticates with multiple principal backends to get an access token with permissions.'
      post 'authenticate' do
        submission = request_body_as_json
        token = authenticate_from_submission(submission)

        if token.nil?
          raise UnauthorizedError, "invalid authentication"
        else
          { token: token.value }
        end
      end

      route_param :authority_name do
        desc 'Accepts a CSR for signing.'
        post do
          authority_name = params[:authority_name]
          authenticate! do |token|
            body = request_body_as_json

            raise NotFoundError, "no authority '#{authority_name}'" \
              unless certificate_authorities.key?(authority_name)
            raise BadRequestError, "no csr provided" unless body.key?(:csr)

            csr = R509::CSR.new(csr: body[:csr])
            common_name = csr.subject['CN']
            raise BadRequestError, "csr lacks common name" if common_name.nil?


            raise ForbiddenError, "Not permitted to create '#{common_name}' " \
                                  "on authority '#{authority_name}" \
              unless token.authorized?(authority: authority_name,
                                       common_name: common_name,
                                       action: 'request')

            signing_opts =
              options_builders[authority_name].build_and_enforce(
                csr: csr,
                profile_name: 'server',
                subject: csr.subject,
                extensions: [], # TODO: do we care about SAN?
                message_digest: 'SHA256',
                not_before: Time.now.to_i,
                not_after: Time.now + Certlord::Settings.certificate_lifespan_in_hours.hours
              )

            cert = certificate_authorities[authority_name].sign(signing_opts)

            register_issued_certificate!(authority_name, csr, cert)
            require 'pry'; binding.pry

            { certificate: cert.to_pem }
          end
        end

        namespace :certificates do
          route_param :certificate_serial do
            desc 'Revokes a certificate by its given certificate ID.'
            delete do
              authenticate! do |token|
                # validity_entry = Models::ValidityEntry.first(serial: serial)

                # raise NotFoundError, "no certificate with serial '#{serial}' found." \
                #   if validity_entry.nil?

                # validity_entry.status = R509::Validity::REVOKED
                # validity_entry.revocation_reason = 0
                # validity_entry.updated_on = DateTime.now

                # validity_entry.save

                # logger.info "Updated '#{certificate.serial}'; now revoked."

                raise NotImplementedError
              end
            end
          end
        end
      end
    end
  end
end
