require 'certlord/error'

module Certlord
  module API
    class APIError < Certlord::Error; end

    class BadRequestError < APIError; end
    class UnauthorizedError < APIError; end
    class ForbiddenError < APIError; end
    class NotFoundError < APIError; end

    class InternalServerError < APIError; end
    class NotImplementedError < APIError; end

    class SigningError < BadRequestError; end
  end
end
