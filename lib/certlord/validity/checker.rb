require 'r509'

module Certlord
  module Validity
    class Checker < R509::Validity::Checker
      def check(issuer, serial)
        raise ArgumentError, "'issuer' must be provided" if issuer.to_s.empty?
        raise ArgumentError, "'serial' must be provided" if serial.to_s.empty?

        entry = Certlord::Models::ValidityEntry.max(:created_on)

        if entry.nil?
          R509::Validity::Status.new(status: R509::Validity::UNKNOWN)
        else
          R509::Validity::Status.new(
            status: entry.status,
            revocation_time: entry.created_on,
            revocation_reason: entry.revocation_reason || 0
          )
        end
      end

      def is_available?
        begin
          DB.fetch("SELECT 1") { |row| }
          true
        rescue
          false
        end
      end
    end
  end
end
