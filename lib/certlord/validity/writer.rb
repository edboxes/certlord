require 'r509'

module Certlord
  module Validity
    class Writer < R509::Validity::Writer
      def issue(issuer, serial)
        raise ArgumentError, "'issuer' must be provided" if issuer.to_s.empty?
        raise ArgumentError, "'serial' must be provided" if serial.to_s.empty?

        raise "'#{serial}' for '#{issuer}' already exists - can't re-issue." \
          unless Certlord::Models::ValidityEntry.first(
            issuer: issuer,
            serial: serial
          ).nil?

        new_entry = Certlord::Models::ValidityEntry.new(
          issuer: issuer,
          serial: serial,
          status: R509::Validity::VALID,
          created_on: DateTime.now
        )

        new_entry.save
        new_entry
      end

      def revoke(issuer, serial, revocation_time = DateTime.now, reason = 0)
        raise ArgumentError, "'issuer' must be provided" if issuer.to_s.empty?
        raise ArgumentError, "'serial' must be provided" if serial.to_s.empty?
        raise ArgumentError, "'reason' must be a Fixnum" unless reason.is_a?(Fixnum)

        new_entry = Certlord::Models::ValidityEntry.new(
          issuer: issuer,
          serial: serial,
          status: R509::Validity::REVOKED,
          created_on: revocation_time,
          revocation_reason: reason
        )

        new_entry.save
        new_entry
      end

      def unrevoke(issuer, serial)
        raise "Unsupported operation. Never unrevoke."
      end

      def is_available?
        begin
          DB.fetch("SELECT 1") { |row| }
          true
        rescue
          false
        end
      end
    end
  end
end
