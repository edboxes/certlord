module Certlord
  module Models
    class TokenRule < Sequel::Model(:token_rules)
      many_to_one :token, class: 'Certlord::Models::Token',
                          key: :token_id

      # TODO: these helper methods are totally inefficient; they should
      #       be replaced with better ones that cache the result and
      #       have a dirty bit for when `rule` is updated.
      def authorities
        JSON.parse(rule)['authorities']
      end

      def actions
        JSON.parse(rule)['actions']
      end

      def common_names
        JSON.parse(rule)['common_names'].map { |i| Regexp.new(i) }
      end
    end
  end
end
