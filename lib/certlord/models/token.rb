require 'certlord/models/token_rule'

require 'uuidtools'

module Certlord
  module Models
    class Token < Sequel::Model(:tokens)

      one_to_many :rules, class: 'Certlord::Models::TokenRule',
                          key: :token_id


      def authorized?(authority:, common_name:, action:)
        raise "authority must be a String" unless authority.is_a?(String)
        raise "common_name must be a String" unless common_name.is_a?(String)
        raise "action must be a String" unless action.is_a?(String)

        rules.map { |r| JSON.parse(r.rule) } \
              .any? do |jr|
                [jr['authorities']].flatten.any? { |a| /#{a}/ =~ authority } &&
                  [jr['common_names']].flatten.any? { |c| /#{c}/ =~ common_name } &&
                  !([ 'all', action ] & jr['actions']).empty?
              end
      end

      def self.generate(content)
        token = Token.new(value: UUIDTools::UUID.timestamp_create,
                          issued_on: DateTime.now).save

        content.each do |rule|
          tr = TokenRule.new

          tr.token = token
          tr.rule = rule.to_json

          tr.save
        end

        token
      end
    end
  end
end
