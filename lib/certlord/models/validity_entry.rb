require 'sequel'

module Certlord
  module Models
    class ValidityEntry < ::Sequel::Model(:validity_entries); end
  end
end
