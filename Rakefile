[ "#{__dir__}/lib", "#{__dir__}" ].each do |lib|
  $LOAD_PATH.unshift(lib) if File.directory?(lib) && !$LOAD_PATH.include?(lib)
end

desc 'Runs Certlord with config in DISKLORD_UNICORN_CFG (default local unicorn.rb)'
task :run_app do
  Dir.chdir __dir__ do
    sh "unicorn -c '#{__dir__}/unicorn.rb'"
  end
end

namespace :db do
  desc "Run migrations with optional version"
  task :migrate, [:version] do |t, args|
    db_migrations_path = "#{__dir__}/db/migrations"

    require "sequel"
    require "certlord/settings"

    Sequel.extension :migration
    db = Sequel.connect(Certlord::Settings.sequel.database_url)
    if args[:version]
      puts "Migrating to version #{args[:version]}"
      Sequel::Migrator.run(db, db_migrations_path, target: args[:version].to_i)
    else
      puts "Migrating to latest"
      Sequel::Migrator.run(db, db_migrations_path)
    end
  end

  desc "create a new migration file"
  task :new_migration, [:name] do |t, args|
    require 'date'
    require 'active_support/core_ext/string/strip'

    raise "name is required" if args[:name].nil?

    filename = "#{__dir__}/db/migrations/#{DateTime.now.strftime("%Y%m%d%H%M%S")}_#{args[:name]}.rb"

    puts "Creating '#{filename}'"

    content = <<-RB
      Sequel.migration do
        change do
        end
      end
    RB

    IO.write(filename, content.strip_heredoc)
  end
end
