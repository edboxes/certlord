[ "#{__dir__}/lib", "#{__dir__}" ].each do |lib|
  $LOAD_PATH.unshift(lib) if File.directory?(lib) && !$LOAD_PATH.include?(lib)
end

require 'sequel'
require 'certlord'

logger Certlord::Logging.logger
worker_processes Integer(Certlord::Settings.unicorn.workers.count)
timeout Certlord::Settings.unicorn.timeout

iface = Certlord::Settings.unicorn.listen.interface
port = Certlord::Settings.unicorn.listen.port

listen "#{iface}:#{port}"

before_fork do |server, worker|
  Signal.trap 'TERM' do
    Certlord::Logging.logger.info \
      'Unicorn master intercepting TERM and sending myself QUIT instead'
    Process.kill 'QUIT', Process.pid
  end
end

after_fork do |server, worker|
  ::DB = Sequel.connect(Certlord::Settings.sequel.database_url) if defined?(Sequel)

  require 'certlord/models'

  Signal.trap 'TERM' do
    Certlord::Logging.logger.info \
      'Unicorn worker intercepting TERM and doing nothing. Wait for master to send QUIT'
  end
end