[ "#{__dir__}/lib", "#{__dir__}" ].each do |lib|
  $LOAD_PATH.unshift(lib) if File.directory?(lib) && !$LOAD_PATH.include?(lib)
end

require 'certlord'
require 'certlord/api/v1'

run Certlord::API::V1
