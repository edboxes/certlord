Sequel.migration do
  change do
    create_table(:validity_entries, engine: 'InnoDB') do
      primary_key     :id,                          type: :Bignum

      String          :authority_name,              null: false,  size: 255
      String          :serial,                      null: false,  size: 255
      String          :csr_hash,                    null: false,  size: 64
      String          :csr,                         null: false,  text: true
      String          :certificate,                 null: false,  text: true

      Integer         :status,                      null: false
      Integer         :revocation_reason,           null: true
      DateTime        :created_on,                  null: false
      DateTime        :updated_on,                  null: true

      index           [ :authority_name ]
      index           [ :serial ]
      index           [ :csr_hash ]
      unique          [ :authority_name, :serial ]
    end
  end
end
