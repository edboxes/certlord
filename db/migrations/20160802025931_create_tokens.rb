Sequel.migration do
  change do
    create_table(:tokens, engine: 'InnoDB') do
      primary_key     :id,                          type: :Bignum

      String          :value,                       null: false,  size: 36
      TrueClass       :revoked,                     null: false,  default: false
      DateTime        :issued_on,                   null: false

      index           [:value],                     unique: true
    end

    create_table(:token_rules, engine: 'InnoDB') do
      primary_key     :id,                          type: :Bignum

      foreign_key     :token_id,
                      :tokens,                      type: :Bignum,  null: false

      String          :rule,                        null: false,    text: true
    end
  end
end
